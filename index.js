import * as ohm from 'ohm-js';
import {extractExamples} from 'ohm-js/extras';
import test from 'node:test';
import assert from 'node:assert/strict';


const notaeDef = `
  Notae {
    Program = number
    number = ( "+" | "-" )? digit+

    // Examples:
    //+ "42", "1", "+10", "-12"
    //- "abc"
  }
`;

const notae = ohm.grammar(notaeDef);

const semantics = notae.createSemantics();
semantics.addOperation('jsValue', {
  Program(num) {
    // To evaluate a program, we need to evaluate the number.
    return num.jsValue();
  },
  number(sign, digits) {
    // Evaluate the number with JavaScript's built in `parseInt` function.
    return parseInt(this.sourceString, 10);
  },
});

test('Notae examples', () => {
  for (const ex of extractExamples(notaeDef)) {
    const matchResult = notae.match(ex.example, ex.rule);
    assert.deepEqual(matchResult.succeeded(), ex.shouldMatch);
  }
});

test('number parsing', () => {
  const matchResult = notae.match('-123');
  assert.deepEqual(semantics(matchResult).jsValue(), -123);
});



