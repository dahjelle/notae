# Notae

Notae is my new, experimental programming language. I've never created a programming language before, so this should be educational.

**Status**: I don't have anything beyond these docs, yet.

# TODO

- [ ] create a ProseMirror install that can handle math? Function definitions?
- [ ] just start parsing _something_ with Ohm.js

# Goals

1. **Learn a lot**. (That shouldn't be hard, since I have no idea what I'm doing.)
2. **Simplicity**. I'd like a language that can be understood top-to-bottom in a small amount of time, and is easy to learn and use for beginners.
3. [**Liveness**](https://youtu.be/VUfZ1sL4aps). Avoid write-compile-run, and aim for editing a running program.
4. **Web-based**, either directly on JavaScript or on Web Assembly or both.
5. **Plays well** with existing web languages and tooling, such as JavaScript modules nad whatever editor you are already using.
6. **Beautiful**, at least to me. I expect this to have some element of experiments with typography being part of the syntax of the language.

# Nice-to-have

- **Performance**. I'd love it to be fast, but not at the expense of any of the goals above. Plus, that's not my expertise.
- **Useful**. My main goal is learning, so I don't expect to make anything actually useful. I'd _like_ to, but it's not my first priority.
- **Good tools**. I have [ideas for tooling](./docs/tooling-ideas.md), but I think that's probably mostly out-of-scope. It will be difficult to avoid scope-creep in this regard.
- **Literate**. I don't expect Knuth's literate programming is everyone's cup of tea, and I don't anticipate making an argument for or against with Notae, but I wouldn't mind exploring the idea some if convenient.
- **Simpler-to-understand numbers** such as [Doug Crockford's DEC64](https://www.crockford.com/dec64.html) — according to him, fast, but can also represent decimal numbers accurately.

# Non-goals

- **Production readiness** I've never done this before. I don't want to do anything to hinder production use, but I can't pretend that I'm making an industrial language.


# Other thoughts

- [nice things about SmallTalk syntax](./docs/smalltalk-appreciation.md)
