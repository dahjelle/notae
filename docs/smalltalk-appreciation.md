# What I like about Smalltalk-80

Here's an example of a Smalltalk-80 program, taken from the inside cover of [_Smalltalk-80: The Language_](https://www.amazon.com/Smalltalk-80-Language-Adele-Goldberg/dp/0201136880):

![A short Smalltalk program](./assets/smalltalk.png)

Some things I notice:

1. **typography** To be fair, SmallTalk was apparently entered a method at a time into the IDE, so there wasn't an official way to separate methods. The bold text for the selector is nice, as is using a proportionally-spaced font. (The serif font for "class name", etc. is a bit odd…)
2. **arrows** I think it's great to use more than just ASCII characters, and the arrows help keep things concise.
3. **minimal punctuation** A period separates statements, and has little visual impact. Colons are used, similar to English, as are semicolons. Square braces are used for blocks, but not every method needs them.

