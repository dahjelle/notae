# Ideas for Notae tooling

- Edit with multiple font sizes/faces/etc. in VS Code.
- Edit "live" in VS Code while interacting with editor
- Debugger and syntax highlighter.
- Language server protocol.
- in-line “expense” heat maps of algorithm complexity
- organize methods arbitrarily on screen next to each other
- 