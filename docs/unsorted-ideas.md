# Previous, undated, thoughts (see the end for the most recent)

- maybe I could start by using ProseMirror in a web page and web components to define syntax, starting with SmallTalk- or JS-like ideas for simplicity?
    - there [are Vim-like keybindings](https://github.com/sereneinserenade/vimirror)
    - this would mean a Notae file is “just” HTML or a subset thereof
    - it would make for easy integration with other web tech
    - how would liveliness work?
    - browser would give one a parser for free! (Enough of a tree for an AST?)
    - eventually probably want it’s own editor or VSCode — but the in-browser version is really appealing to get started
    - very tempting to try SmallTalk-like, just for fun
    - do I need Ohm for parsing function bodies?

## Typography

Not just looks nice, but legibility. (Even for older eyes.)

- fonts
    - [Atkinson Hyperlegible](https://brailleinstitute.org/freefont)
    - [Go fonts](https://go.dev/blog/go-fonts) — DIN 1450 compatible (a German standard?)
    - [Linotype options](https://www.linotype.com/6990/din-1450.html) — DIN 1450 but expensive 

- reference manuals are carefully organized for "dip-in, dip-out" kind of information like we tend to treat debugging: get a stack trace and go to just that section of code
- 

 


- immediate-mode GUI
- Rebol's terse syntax for GUIs (how?)
- Smalltalk variables should be objects and accept messages
- classes vs prototypes
    - classes sure seem more natural — or maybe just more familiar?
    - maybe JavaScript's solution of class syntax over prototypes is actually smart?
- fixed-place decimals (i.e. Crockford's DEC64)
    - [discussion on DEC64](https://news.ycombinator.com/item?id=7365812)
    - another [reference on decimal types](https://speleotrove.com/decimal/)
    - [posits](https://www.sigarch.org/posit-a-potential-replacement-for-ieee-754/)
- SmallTalk-like inspectable
    - database of what references what
    - dig all the way down to primitives
- good error messages
- beginner friendly
- GUI layout?
    - how does Red or Rebol do it?
- Imperative idioms but functional patterns?
- SmallTalk that looks like JS?
- Content-addressable like Unison?
- could you remake VB with a subset of HTML, JS, CSS and perhaps some official widget set?
- "little things with great love"
- improvement to browser/VS Code dev tools?
- thin layer over JS?
- state charts
- how does WebPack hot reloading work?
- can I make (or is there?!?) a VS Code plugin that would compile & run WebAssembly in VS Code? (start for an IDE)
- don't allow messaging to nil
- [capabilities](https://borretti.me/article/how-capabilities-work-austral) or [permissions system like Deno](https://deno.com/manual@v1.28.3/basics/permissions) ([discussion](https://lobste.rs/s/qbeqio/how_capabilities_work_austral)) 
- I think it was Wirth that dictated that "the time to compiler the compiler could only go down, not up" as a metric for limiting complexity?
- does a language need declarative and "imperative" modes?
- I don't know that much about typography, but I'd love a language that could look _nice_
- is Erlang message-passing from SmallTalk but in a functional style instead of OO?
- what would SmallTalk look like with import/export and modules instead of just classes?
- looks like SmallTalk's class `Behavior` has a lot of the "compile this string and add the compiled method to this class" stuff
- could one make a SmallTalk with syntax for declaring some things "pure" objects (no mutating internal state) and other things that the internal state matters?
- one of the challenges with SmallTalk is very much the class hierarchy. I don't know that the problem is non-existent in functional languages, due to the expression problem duality, but it's pretty clear that the SmallTalk-80 built-in classes were expertly designed, and it is unlikely for a newbie to manage to do the same
    - part of the "problem" is that so many classes end up having to know about each other — very convenient, but also pretty coupled, in my opinion
- async/await semantics available for messages
    - what does Pharo do?
    - caller can always(?) choose to wait for result or do action whenever
    - are more complicated scenarios necessary?
    - can all functions be async in semantics? When does it matter for other code to run in the thread? How can that be addressed?
- could you do Rust’s ownership model(?) in a dynamic lang? Certainly not high priority…but interesting. 
- I think the reason I don't hate OOP is that I am _really careful_ about state — minimize object state, pure functions as much as possible, limit the mutating of state variables as much as possible, etc. Can a language be designed to encourage that kind of architecture?
    - limit the number of instance variables available per object?!?
- interesting minimal syntax ideas: https://github.com/fastn-stack/fastn/
- here’s a language that tried to use typography in syntax, but it isn’t very pretty: https://www.zifyoip.com/wysiscript/sigbovik.pdf
    - I see that was submitted to a joke conference, which isn't surprising given its output. Funny joke, but is there really nothing _real_ in this space?
- [7 minutes of Pharo Smalltalk for Rubyist](https://youtu.be/HOuZyOKa91o) video
- argument for [starting with objects not classes](https://lobste.rs/s/mjhujr/expressivity_limitations_object#c_91dems)


Something that compiles to WebAssembly and you can see “all the way down”? Something that is a thin layer on top of JS and simplifies the web stack? (Is that a necessary tension between simple and immediately useful?)

It’d be amazing to do the next Icon tools, incidentally, but that is less of a programming language as much as a framework or something. I don’t know if it’d get used. 

Did I read that a Forth implementation of WebAssembly is 14k lines? And Forth is pretty low level, isn’t it? Still…may be worth researching. Mostly, I need something really small. 

What is the simplest syntax possible? Does Unicode give enough characters to avoid escaping issues? 

I think I just want to make a language as simple as possible. Maybe not useful. 

Can one define a parser 

---

# 2023-07-10

I'm going to start organizing chronologically. This may someday need to be split into separate files or something, but for now, this will do. I think the chronological organization will help me see progression of my thoughts over time in a way that categorizing them doesn't.

Had an interesting discussion on the [Future of Coding Slack](https://history.futureofcoding.org/history/weekly/2023/07/W2/?fromDate=2023-07-05&toDate=2023-07-11&channel=thinking-together&filter=#2023-07-08T22:54:43.682Z) about fonts in code. I think some grasped some of my ideas, but others too quickly related the idea to literate programming — which is related, perhaps, but distinct.

One good comment was how prose is inherently linear, whereas only small sections of code typically are.

Another was that much of the hard part about reading code is about its structure. I fully agree. How can we communicate that more effectively to a person? (I suspect visual aspects help, even if they aren't the whole story.)

One person hacked up an Emacs demo. :-D Cool! (I really want something _beautiful_, though.)

It certainly looks like I might be building some sort of editor. I don't want to. But it sure seems like editor and language are pretty tightly intertwined.

Some other thoughts:

- is there some sort of automation or simple scripting that could still use a language? (attempting to find a practical implementation)
- an editor that edited a whole project as one long file, but automatically could split up classes, modules, whatnot into files for version control (I bet Pharo is doing something like this…)
- maybe my editor lets me turn on/off visibility of code blocks with checkboxes on an outline or something — even reorganizing to put related bits of code next to each other
- should this start life as a VS Code plugin?
- while not my expertise, maybe a environment for rapid prototyping GUIs would be an interesting target? (statecharts?)

---

# 2023-07-20 (or so)

There **has** been a programming language using formatting as syntax: [ColorForth](https://web.archive.org/web/20131001052225/http://www.complang.tuwien.ac.at/anton/euroforth/ef01/moore01a.pdf). Apparently, it used 4 colors (or 4 typographic variations) to determine whether a word was a definition, to compile it, to execute it, or it was a comment. Fascinating, but not exactly the stuff I was thinking.

----

# 2023-07-29

(copied from my notebook)

- subscripts and superscripts are _great_ formatting
    - or boxes around indices?
- are dictionary keys and local variables similar constructs? they are by implementation — should they be similar in syntax?
    - probably both use ← for assignment
- is indentation sufficient to implement blocks? what does Python do for closures?
- JS' function-in-object-key syntax feels nice, but I'm not sure that it is all that helpful (i.e. instead of `foo: function(a)` you can do `foo(a)`)
- Rebol's variety of data _types_ (primitives — i.e. dates and URLs, etc.) as literals without constructors is awesome
- what were the Clojure data types? it is strange to me how few built-in data structures (with literals) there seem to be in most languages
- what font & formatting variations could be used?
    - font size
    - bold, italic, weights
    - face: slab, serif, sans-serif, cursive? (is too many too hard to read?)
    - super- & sub-script
    - underline and squiggly/curly underline
    - background color
    - text color
    - is outline text still a thing? (looks like it is [`text-stroke`](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke))
    - box around text
    - margin notes, footnotes, endnotes
- can use `=` for comparison if `←` is used for assignment
- interacting with data structures is pretty important
    - seems like iterator protocols + key access protocols get pretty far — though the JavaScript `for`-loop over objects is still rather clumsy
- does Rebol/Red allow for user-defined parsing extensions? how?
- how many of my ideas are familiar only with a math background?
- is class or prototypical object-orientation "better"? what's simplest?
- formatting to distinguish between string literals and variable names
- do clojure's data structures usually have value equality of reference equality?
- boolean operators — how did SmallTalk do this again?
- modules vs. classes vs. files

----

# 2023-08-18

Via [the Future of Coding Slack](https://linen.futureofcoding.org/t/14138692/hi-everyone-designing-the-future-of-coding-i-m-stan-a-develo#fca11bf2-b2c2-4aae-9687-4cb17308cff5), I found some interesting tidbits. First, [an essay by wanderingstan](https://static.main.linendev.com/attachments/98d8fc94-0576-4fbc-8b18-4afb8b12d36a/c91e3677-b962-4dae-9f7c-b385cd6d063cvisual-design-of-code-wanderingstan.pdf) with many similar thoughts on typography to my own, but, further, **an entire book** on the topic of using typography in code: [_Human Factors and Typography for More Readable Programs_](https://archive.org/details/humanfactorstypo0000baec/) by Baecker and Marcus. Definitely ordering the book!

I learned about [SOM](https://som-st.github.io/) — it's a simple SmallTalk derivative used for learning about virtual machines. While I haven't yet seen a language spec, it's interesting that it is a SmallTalk derivative. I don't know that it has any benefits relative to any other SmallTalk, though.

Interesting in another way is this [article about why WordStar was popular with writers](https://www.sfwriter.com/wordstar.htm). There is perhaps more to digest there, but I think the key insight to me was:

> I've concluded that there are two basic metaphors for pre-computer writing. One is the long-hand manuscript page. The other is the typewritten page. Most word processors have decided to emulate the second — and, at first glance, that would seem to be the logical one to adopt. But, as a creative writer, I am convinced that the long-hand page is the better metaphor.
>
> Consider: On a long-hand page, you can jump back and forth in your document with ease. You can put in bookmarks, either actual paper ones, or just fingers slipped into the middle of the manuscript stack. You can annotate the manuscript for yourself with comments like "Fix this!" or "Don't forget to check these facts" without there being any possibility of you missing them when you next work on the document. And you can mark a block, either by circling it with your pen, or by physically cutting it out, without necessarily having to do anything with it right away. The entire document is your workspace.

Now **that's** kind of fascinating. He was writing specifically about prose writing, but I wonder if some of his insights apply similarly to writing code? We have a few of these facilities available — making comments as we go — but I get the impression that WordStar offered a _lot_ of functionality in that direction. I'd like to learn more.

# 2023-08-24

I think the key with WordStar is that it allowed you to move most of the _process_ into muscle memory so that you could operate without distraction and keep your fragile ideas from floating away — at least, that's my thesis.

For instance, since keyboard sequences can end in muscle memory, imagine the ability to shuffle ideas into various spots in a single (or multiple?) documents essentially as fast as you can type, without having to think about hand-eye coordination (assuming you can touch-type) or labelling files. I experienced something like this with the Things to-do app — a global keyboard shortcut to quickly enter new ideas to be sorted later was amazing, as I never had the overhead of opening a different app, creating a new task, etc.

---

I think the SOM language examples were initially confusing to me because they defined functions like:

```
function = (
    ... body ...
)
```

It didn't occur to me until today that such a syntax is necessary for SmallTalk when you don't have a GUI for the thing like Pharo or SmallTalk-80 that let you just define function bodies.

---

While I'm not sure what an "ideal" IDE for Notae is like, I do think the ability to immediately run code and "lively"ness is important.

MS Basic on the early Macs had (if I recall) separate graphics and text output windows? (I should look that up.) I like the idea of a transcript for text output.

Given Notae's emphasis on visual presentation, maybe it should (essentially) default output into HTML, appending to the bottom of a `<body>` tag or some such — allowing at least some of the HTML features.

# 2023-08-25

Would it be possible to have _types_ in a language add syntax and parsing for their own _literal representation_? (Perhaps this is what Red/Rebol already do for all of their built-in data type literals.)

I don't know enough about parsing and how it works to even know if that is possible.

# 2023-08-29

The [Crumb programming language](https://github.com/liam-ilan/crumb) is a minimal LISP. Might be some good inspiration there as to what things need to be in and what needs to be out of a small language.

# 2023-09-08

An idea: <span style="background-color: yellow">use something like a highlight</span> to indicate expressions that ought to be logged when running the code. This makes it easier to log things for debugging without having to restructure the code to create a variable to log.

# 2023-10-05

A language that may be using some typographical features: [Wordplay](https://wordplay.dev) and its [specification](https://docs.google.com/document/d/1pTAuU0qyfp09SifNUaZ_tbQXbSgunSLfZLBRkeWf_Fo/edit#heading=h.l2lzf5ih38l1).

# 2023-10-11

The end of [DJB's talk on optimizing compilers](http://cr.yp.to/talks/2015%2E04%2E16/slides-djb-20150416-a4.pdf) has an interesting suggestion: perhaps an optimizing compiler could interact with a programmer about which optimizations to perform on a given bit of code.

More interesting for Notae, which I don't expect to get into optimizations much, is that expanding code beyond plain text allows things like formatting that could indicate constraints about a bit of code that could help indicate to a interpreter or compiler some extra information about a bit of code that aren't obvious from the code.

Either way, it seems that having some way of having a system that is understandable from top-to-bottom (like Pharo's interpreter is, as I understand, written largely in Pharo) is the kind of direction I'd like to go.

# 2023-10-16

Fun: an [attempt to use font formatting in AI image generation prompts](https://rich-text-to-image.github.io) to indicate to the AI colors and styles of various elements of the prompt.

Interesting: [cursorless: a spoken language designed for editing code](https://www.youtube.com/watch?v=NcUJnmBqHTY). Some really interesting programming language design bits, and a very fine talk, too.

# 2023-10-25

Notes on Crockford's new [Misty programming language](https://www.crockford.com/misty/):

- message passing/actor model **in addition to** regular function calls
- preconditions and postconditions for design-by-contract ("The verifiable requirements can be much more effective than typechecking in discovering interface confusions and other bugs.") ([an alternate view](https://spencerfarley.com/2023/04/07/dbc-vs-type-driven/))
- no reserved words
- optional immutability features
- strict formatting
- uses some Unicode symbols for certain features
- looks like only infinite loops with break(!)
- function bodies cannot modify parameters

# 2023-12-04

A [post on Twitter](https://twitter.com/antumbral/status/1730829756013375875) made me think that the `<ruby>` tag may be useful.

# 2023-12-12

[Someone pointed out how to nest code structure with comments.](https://www.pathsensitive.com/2023/12/should-you-split-that-file.html)

```javascript
/*******************************************************
 **************** h1 in C/C++/Java/JS ******************
 *******************************************************/

/**************
 ********* This is an h2
 **************/


/********
 **** An h3
 ********/
```

What if a language support such a feature from the get-go?

Also, some [notes about a decade of building a language](https://yorickpeterse.com/articles/a-decade-of-developing-a-programming-language/):

- Avoid gradual typing
- Avoid self-hosting your compiler
- Avoid writing your own code generator, linker, etc
- Avoid bike shedding about syntax
- Cross-platform support is a challenge
- Compiler books aren't worth the money
- Growing a language is hard
- The best test suite is a real application
- Don't prioritize performance over functionality
- Building a language takes time

I may or may not agree with all the points, but I'm always interested in advice from experience.

In particular, "the best test suite is a real application." I would really like a real application for Notae. But I don't know what.

# 2023–12-14

A [talk on bidirectional type checking](https://www.youtube.com/watch?app=desktop&v=utyBNDj7s2w). 

# 2023-12-20

An [interesting paper](http://recherche.enac.fr/~conversy/research/papers/onward2014-langVis.pdf) about how to try model the visual perception of textual and visual programming languages.

# 2023-12-21

Probably not a problem for Notae, but I was thinking the other day about the challenges of event-driven programming. Have any programming environments addressed them?

As a co-worker expressed it, "When I write JavaScript, I keep ending up with timing issues." It wasn't the kind of "timing issues" that one gets with multiple threads. Some of it was the kind of timing issues inherent to asyncronous programming: which network request will return first? But **most** of it seemed to be the kind of "timing issues" endemic to event-driven programming: when the user makes (say) a mouse click, it triggers a _series_ of events in the browser, (say) `mousedown`, `mouseup`, and `click`. (I believe there are often more.) Then, those events are responded to by elements in the DOM bubbling all of the way up from the DOM element first interacted with up to the window and back. At each step, the program could be using `preventDefault` or `stopPropagation` to modify the typical behavior. The programmer needs to know:

1. what events are emitted by the user's interaction
    - this may also include events handled/modified by third-party code in use!
2. the tree structure of the DOM
3. which elements in that tree structure are monitoring for which events
    - again, this can be in other code than the code in question!

The browser DevTools that I've looked at capture (2) elegantly. The information for (3) is usually present, but it can take quite a bit of work to inspect the entire DOM tree for event handlers. Third-party or minimized code can make that more challenging, and it is often further complicated by development frameworks that add other event handlers.

However, more fundamentally, the challenge is that **tracing execution is difficult from examining the source code**! This is especially in contrast to the linear execution of code within most function bodies or even in the back-and-forth dance done between front-end and back-end code.

Whether or not you think JavaScript's `async`/`await` was a good idea in general, its main accomplishment for many programmers was taking the nested asynchronous callback code and making it appear as if it was linear. It could (with some exceptions like mutation of global variables) be treated _as if_ the code was executing linearly.

It sure _feels_ like there is a better way, but I don't know what it is.

Part of the issue is admittedly _inherent_ to the problem: when user interaction can occur at any time, and a program might care about different parts of that interaction at different places, things are necessarily complicated.

Part of the problem is _tooling_: tracing events flowing through the system is far more difficult than tracing network requests for no reason that I'm aware of. Why can't I see what events are triggered by my interaction — or, at least, which events the program is actively monitoring for? Why can't I step through just the code that is responding to a user interaction (and ignore unrelated code also on the event loop? Or are these features available already in one of the browser's DevTools and I just don't know how to use them?

That said, the problem reminds me of how immediate-mode rendering has taken over the UI world: mapping a state to a desired UI given that state is often a much easier way to reliably model user interfaces. Is there a similar pattern that could be used in event handling?

# 2023-12-18: Interesting Languages and Tidbits

Just brainstorming some things that I've seen around and appreciate in other languages.

I like the _idea_ of a type system helping to find bugs, but for a language that is meant to be _playful_, having to spend time finagling types before getting something done. I'm more inspired by _liveness_ and _playfulness_ than correctness.

Notebooks are interesting in the sense of experimenting and playing, but there is a disconnect between their potential out-of-order execution and the appearance of the notebook.

The [Lax programming language](https://github.com/swedishvegan/complax) uses pattern matching with "holes" to allow some interesting function signatures:

```
function { stuff with {a} and {b} } { return a + b }

start program { 
    
    output stuff with 1 and 2    // Calls the function "stuff with {a} and {b}" with a=1 and b=2
    output "\n"    // Newline
    
}
```

As you might imagine, there is a [whole section of the documentation about dealing with ambiguity](https://github.com/swedishvegan/complax#pt3.5).

---

Red/Rebol has a whole bunch of [datatypes built-in to the language](http://www.rebol.com/docs/core23/rebolcore-16.html) as primitives to the language, including emails (with _no extra punctuation_)

```
send luke@rebol.com {some message}
```

files (with a bit of punctuation, but still, notably, _not strings_)

```
save %this-file.txt "This file has few words."
```

URLs

```
probe url? ftp://ftp.rebol.com/
true
```

and more. I don't know enough about parsing to know exactly how the language can pull that off, but considering how awful putting SQL into strings has been, having some language primitives for these things that have become very common in everyday use sounds like a fantastic idea. There are also quite a few more.

---

HyperCard is interesting because it has such a gradual on-ramp: you can start and do useful things with the drawing tools. You don’t have to code until much later. Interestingly, despite its reverence by many, it did not allow making Mac-standard apps. 

---

[Decker](https://beyondloom.com/decker/) has a lot of HyperCard similarities, but is a masterclass in design tradeoffs. It is simple and approachable, at least as long as you are content with ASCII and black-and-white and a small screen. Within those constraints, however, it has data grid GUI components and a scripting language that has a built-in query language. 

---

Is it better to start with a language like Lox from _Crafting Interpreters_ and add a bit of formatting? Or just start from scratch? The formatting is absolutely the biggest thing I want to try, but there are so many fascinating ideas out there!

---

Doug Crockford’s [Misty programming language](https://www.crockford.com/misty/functions.html) allows pre-conditions and post-conditions for functions instead of a type system. (Another article on [design by contract](https://tigerbeetle.com/blog/2023-12-27-it-takes-two-to-contract/).)

---

On a bit of reflection, I think the Lax programming language (the one that uses templates to define functions) is kind of a text-only version of the visual block programming languages (like Scratch or Block.ly) that have programmers fill in holes in blocks to program. I guess without the structured editing approach.

# 2023-12-29

With a formatting-centric approach like I'm still thinking with Notae, I'm starting to think that the main thing I need to think about is the editing approach. If formatting is central to the language, it _needs_ to be easy to edit. As easy as text for the text bits (eventually with most of the features of a standard text editor) and as easy as a word processor for the formatting bits (with key sequences to make it easy).

I don't think I can get away from an integrated editor/runtime environment. They need to be kept as simple as possible to be possible for me to be able to accomplish anything. But given the sort of values that keep coming up in my mind — such as _liveness_ and _formatting_ — it seems that they are parts that must go together.

I'm not sure if I'd gain anything by starting in a VS Code extension or if just a regular web page would be the best starting point.

- - -

Another interesting inspiration is [Val Town](https://www.val.town). While it uses JavaScript, its intent is to have little bits of code that can be accessed via URL or on a timer. It gets rid of a ton of complexity for those things. 

It is tempting to start with building servers, but it’s probably smarter to start building small GUI apps. Though that may be harder…

# 2023-12-31

Experimenting some with ProseMirror, I see that ProseMirror manages "inline content" — i.e. formatting of inline tags like `<b>` and `<span>`, etc. — slightly differently than HTML. Rather than the internal representation being nested, each stretch of formatted text is metadata. To swipe their example from [the ProseMirror guide](https://prosemirror.net/docs/guide/#doc.structure), HTML would have this sort of nested structure:

```html
<p>
    This is 
    <strong>strong text with 
        <em>emphasis</em>
    </strong>
</p>
```

ProseMirror, on the other hand, would store this as something more akin to (my syntax, not theirs):

```html
<p>
    <span class="">This is</span>
    <span class="strong">strong text with</span>
    <span class="strong em">emphasis</span>
```

From their docs:

>>>
This more closely matches the way we tend to think about and work with such text. It allows us to represent positions in a paragraph using a character offset rather than a path in a tree, and makes it easier to perform operations like splitting or changing the style of the content without performing awkward tree manipulation.

This also means each document has one valid representation. Adjacent text nodes with the same set of marks are always combined together, and empty text nodes are not allowed. The order in which marks appear is specified by the schema.
>>>

I don't (yet) know if this implies anything for Notae, but it sure seems worth noting here. Obviously ProseMirror knows how to _output_ that as a tree structure — I think?

---

It definitely looks like the "schema" that ProseMirror allows one to configure is pretty powerful as far as letting one express exactly what content can be generated and how it can be nested.

---

Interesting. Of course, ProseMirror needs to be able to represent its content in HTML. So each ProseMirror node needs to be able to output a form of HTML. Interesting.

# 2024-01-02

I've been wanting a "target app" for this language for some time. I was listening to a podcast about some of the first text editors this morning — and it made me wonder if a language for modifying text, like `grep` or `sed` or `awk` or something, but more approachable, would be interesting. I'll have to think about that some.

I suppose another could be something to like a query language or a modify tree structures language or a modify-AST language.

# 2024-01-03

That said, something that sends messages back-and-forth across a network sounds more _fun_, even if I can't see a practical use for it. We shall see.

---

I had a couple questions this morning that I don't know the answer to:

1. Smalltalk's style is "send a message to the system to have it do stuff." React's style is "the system sends a message to you (i.e. `render`), and then you reply with what the current HTML ought to be. Are these patterns in tension with each other, mutually exclusive, or compatible?
2. Some languages have types, and some languages have constraints (i.e. specify pre- and post-conditions for functions). Constraints are presumably more powerful than most type systems, but I don't know if they allow for the same level of compile-type verification and correctness that type systems currently do. Or tooling, for that matter.
3. I recently read a paper called "[The interactive nature of computing: Refuting the strong Church-Turing Thesis](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=2e8b3077ea8fbcf36b46cfd3b93fcf43db74aaa3)" that basically said a Turing machine was fine for algorithmic programs, but most programs today are interactive, which requires another class of analysis. Further, the [Objective-S language](https://objective.st/About?ref=upstract.com) says, "What we currently call general purpose languages are actually domain specific languages for the domain of algorithms." That's interesting. Not sure what to make of that, either.

---

You know, if I'm really thinking of the formatting being an integral part of things, maybe I need to go whole-hog and allow images in comments. Perhaps even embedded draw.io or some such?

That would be pretty amazing, actually, for some things.

# 2024-01-09

Actually, I wonder if writing a language just to focus on [7GUIs](https://eugenkiss.github.io/7guis/) wouldn't be a bad place to start, even if just thinking about the two simplest tasks, the counter and the temperature converter. I think that might be just enough "practical application" to give me some direction?

I'll admit, it is interesting to me that the ["data persistence" problem](https://linen.futureofcoding.org/t/16236926/so-i-ve-only-been-a-real-developer-again-for-a-month-but-hol) isn't really solved in development — i.e. why do we have to write so much code to sync the front end (whether web or native) to a persistence layer. I don't know that should be something for Notae to tackle, but it is interesting to me.

# 2024-01-10

I see Mathematica has a bit of [formatting in its code editor, at least headings and disclosure triangles](https://writings.stephenwolfram.com/2024/01/the-story-continues-announcing-version-14-of-wolfram-language-and-mathematica/#for-serious-developers). If I recall, one can also insert rich text into notebooks, with a [lot more options](https://reference.wolframcloud.com/language/guide/NotebookFormattingAndStyling.html) than the Markdown of Jupyter notebooks.

# 2024-01-11

Abstraction boundaries to create modules is really useful for technology development. The module boundaries are good, but not being debug across the module boundaries is bad. (i.e. I have no idea what assembly my JavaScript produces) (i.e. connecting logs from multiple microservices together) (i.e. ) (i.e. [why is the virtual machine slow?](https://arxiv.org/abs/2312.16973))

# 2023-01-16

An [interesting idea to use `label:` to mark off blocks of code](https://www.codesections.com/blog/label-your-code/) in languages that support it. It even suggests using blocks in JavaScript — putting curlies around blocks of code to indicate the lifetime of certain variables. It also says, "comments should usually be complete sentences [that] start with a capital letter [and] end with a period." Interesting.

# 2024-01-26

Here's an [interesting programming language for children](https://www.youtube.com/watch?v=dGvG2wCTJK8). Apparently, you can define your own operators with behavior. Still an early prototype, but I'd love to see more about it.

# 2024-07-16

(Yes, it's been a while.)

I believe the language above is the one [discussed in this paper, called Representar](https://dl.acm.org/doi/abs/10.1145/3563835.3567660). It has 4 basic "cards" — tiles or "characters" in a text-based language — but the user can draw on their own cards to create a graphical programming language. It can do a surprising amount of stuff with just substitution, categorization, conditional, and wildcard cards: evaluation, math, simulations, and (adding a bunch more cards for robot control and sensing) controlling a robot.

I have no idea how much one could accomplish with a language this simple. It sure _looks_ like fun, though: scribbling on the tiles to make a new concept, and then defining it.

# 2024-07-17

From [We need visual programming. No, not like that.](https://blog.sbensu.com/posts/demand-for-visual-programming/):

>>>
Developers do spend the time to visualize aspects of their code but rarely the logic itself. They visualize other aspects of their software that are _important_, _implicit_, and _hard to understand_.

Here are some visualizations that I encounter often in serious contexts of use:

- Various ways to visualize the codebase overall.
- Diagrams that show how computers are connected in a network
- Diagrams that show how data is laid out in memory
- Transition diagrams for state machines.
- Swimlane diagrams for request / response protocols.

This is the visual programming developers are asking for. Developers need help with those problems and they resort to visuals to tackle them.
>>>

---

I saw a post from the [Moonbit language for WebAssembly](https://x.com/moonbitlang/status/1811717139839221766) that showed a `..` operator for chaining mutable calls — kind of like a "fluent" JavaScript interface, but in syntax rather than provided by a library.

```
fn main {
let array: Array[Int] = [4, 5, 1, 3, 1, 2, 4, 5]
    array.dedup()
    array.push(8)
    array.push(7)
    array.retain(fn(x: Int) -> Bool { x > 4 })
    array.sort()
    array.each(println)
}
```

converted to

```
fn main {
    // Use cascade operator!
    [4, 5, 1, 3, 1, 2, 4, 5]
        ..dedup()
        ..push(8)
        ..push(7)
        ..retain(fn(x: Int) -> Bool { x > 4 })
        ..sort()
        ..each(println)
}
```

I believe SmallTalk-80 used a `;` for the same thing — send another message to the same object.

My thought was: what if you could do something like this instead:

![show the cascade with arrows and styled numbers](assets/cascade.png)

It's the sort of thing that could be automatically be applied by an IDE regardless of the backing syntax.

---

Just heard of [Rye](https://ryelang.org), which has some inspiration from Rebol and looks interesting. 

> Load a webpage, save it to a file, print out all the links.

```
get https://ryelang.org |write* %page.html
|reader |parse-html { <a> [ .attr? ‘href |print ] }
```

I think I mostly like the idea of built in data types like URLs. The HTML parsing is cool, too, but a lot harder for one person to pull off. 

# 2024-07-19

Weird thought this morning: I think I want a language/environment to replace Microsoft Access, at least in a small way. That could:

- use SQLite for storage
- have a rich-text programming language
- local-first
- HyperCard/SmallTalk-like image environment
- 
- I think it is probably somewhat approachable with web technologies. Is there a market? I have no idea. There doesn’t need to be a big one. Interestingly, it could be the basis of church software or the like. It’s not far from Decker or something. Hmmm…

# 2024-07-10

I'm finally catching up on some old browser tabs, such as this [Reddit discussion on rich-text source code](https://old.reddit.com/r/ProgrammingLanguages/comments/1b542oa/richtext_source_code/). That sounds like much closer to what I've been thinking than anywhere else I've read! The usual discussion comes up:

- I prefer plain text.
- We have all this tooling supporting plain text and not rich text.
- Literate programming failed.
- We already have tools to typeset existing languages.
- What about notebooks?
- RTF is too messy.

And those are some good points! Well, except the literate programming one — I'm not sure why "rich text source code" seems to always bring in the idea of literate programming. I get that Knuth's examples had TeX-formatted comments, but the code bits (as far as I'm aware) were regular plan-text code. Looking back at the comments, though, there was at least one fan of actual literate programming!

Further, I'm interested in using formatting _as part of the syntax_ of the language. In other words, it is how one expresses the code. As a simple example (not necessarily one I might use, I don't know), in most languages, identifiers have some limitations. Most commonly, you can't use spaces. Why not? I'm not 100% sure, but either because white space is used to separate tokens or because it makes parsing simpler (possible?). What if you had a rich-text marker (for simplicity, like an `<n-id>my indentifer goes here</n-id>` HTML/XML tag) that surrounded the identifier? You exchange one limitation — lack of spaces — for another: having to include the surrounding tags all the time. (I think that the trade-off could be somewhat mitigated with some good keyboard shortcuts and editor tooling.)

A few links and information I didn't know before:

- A few languages (in addition to colorFORTH) that had rich-text source:
    - [Oberon3](https://github.com/rochus-keller/OberonSystem3) — I've seen Oberon, but did not know this feature. Was it syntax, or just presentational?
        - I wonder if [this screenshot](https://camo.githubusercontent.com/ed818fe7ffc3a67b82668b1c8672f7771a10e3bccd392280a7e64ede88f0c465/687474703a2f2f736f6674776172652e726f636875732d6b656c6c65722e63682f6f6265726f6e5f73797374656d5f335f6f627870616c5f322e332e365f6d696e696d616c5f73797374656d5f342e706e67) shows an example on the middle pane on the right-hand side? Italic for comments, bold for something?
    - A math programming language and editor called [Forscape](https://github.com/JohnDTill/Forscape).
    - Someone claimed that "[Algol68 was originally made to be coded via basic rich text](https://old.reddit.com/r/ProgrammingLanguages/comments/1b542oa/richtext_source_code/kt3vkfy/)" which sounds very interesting, though they didn't link to any examples.
    - Mathematica notebooks, which is a fair point. I don't remember how much the formatting was optional versus required.
    - Guy Steele's [Fortress](https://www.cs.tufts.edu/comp/150FP/archive/neal-glew/mcrt/Fortress/1.02_steele.pdf) programming language — though, again, the formatting was limited to math, as far as I can tell.

Again, all told, not too many efforts to use formatting as syntax to imply semantics (outside of math and colorFORTH).

I suppose, another aspect of this is _audience_. If one doesn't target professional programmers, but rather hobby programmers, then some of the tooling questions go away. I'm not advocating for not being able to use GitHub or the like! I am saying that the plain-text version of code doesn't need to be as readable as Markdown, just as readable as JSON, HTML, or XML. (Maybe reStructuredText or AsciiDOC — neither of which I know anything about — is another reasonable middle ground?)

# 2024-07-26

Ran across [Haystack Editor](https://haystackeditor.com) today. Like The ["Code Bubbles"](https://www.researchgate.net/profile/Steven-Reiss-2/publication/221519599_Code_bubbles_A_working_set-based_interface_for_code_understanding_and_maintenance/links/02e7e51b614b6c8e0f000000/Code-bubbles-A-working-set-based-interface-for-code-understanding-and-maintenance.pdf) idea, it tries to allow you to arrange code in a 2D workspace, including call graphs.

Also ran across [Ilograph](https://www.ilograph.com), a diagramming tool. The interesting thing is that it lets you "zoom" by removing levels of detail from the diagram.